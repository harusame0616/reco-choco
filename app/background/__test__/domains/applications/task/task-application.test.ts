import { TaskApplication } from '../../../../domains/applications/task/task-application';
import { OMTaskRepository } from '../../../../domains/infrastractures/on-memory-repositories/om-task-repository';

let taskRepository: OMTaskRepository;
let taskApplication: TaskApplication;
beforeEach(() => {
  taskRepository = new OMTaskRepository();
  taskApplication = new TaskApplication(taskRepository);
});
describe('task register', () => {
  test('min length name', async () => {
    const taskName = 'a';
    await taskApplication.register(taskName);
    expect(taskRepository._tasks[0].name).toBe(taskName);
    expect(taskRepository._tasks[0].order).toBe(0);
  });

  test('multiple register', async () => {
    for (const [i, taskName] of ['a', 'b', 'c', 'd'].entries()) {
      await taskApplication.register(taskName);
      expect(taskRepository._tasks[i].name).toBe(taskName);
      expect(taskRepository._tasks[i].order).toEqual(i);
    }
  });

  test('max length name', async () => {
    const taskName = '12345678901234567890';
    await taskApplication.register(taskName);
    expect(taskRepository._tasks[0].name).toBe(taskName);
  });

  test('zero length name', async () => {
    await expect(taskApplication.register('')).rejects.toThrow();
  });
  test('over length name', async () => {
    await expect(taskApplication.register('123456789012345678901')).rejects.toThrow();
  });

  test('duplication name', async () => {
    await taskApplication.register('task');

    await expect(taskApplication.register('task')).rejects.toThrow();
  });
});

describe('task list', () => {
  test('normal', async () => {
    const taskNames = ['a', 'b', 'c', 'd'];
    for (const taskName of taskNames) {
      await taskApplication.register(taskName);
    }

    const tasks = await taskApplication.list();
    expect(tasks.length).toBe(taskNames.length);

    tasks.forEach((task, i) => expect([task.name, task.order]).toEqual([taskNames[i], i]));
  });
});

describe('task update', () => {
  let id: string;
  beforeEach(async () => {
    await taskApplication.register('a');
    ({ id } = taskRepository._tasks[0]);
  });

  test('full specified', async () => {
    const name = 'newTask';
    const order = 5;
    await taskApplication.update(id, { name, order });
    expect(taskRepository._tasks[0].name).toBe(name);
    expect(taskRepository._tasks[0].order).toBe(order);
  });

  test('only name specified', async () => {
    const name = 'newTask';
    await taskApplication.update(id, { name });
    expect(taskRepository._tasks[0].name).toBe(name);
    expect(taskRepository._tasks[0].order).toBe(0);
  });
  test('min length name', async () => {
    const name = 'a';
    await taskApplication.update(id, { name });
    expect(taskRepository._tasks[0].name).toBe(name);
    expect(taskRepository._tasks[0].order).toBe(0);
  });

  test('max length name', async () => {
    const name = '12345678901234567890';
    await taskApplication.update(id, { name });
    expect(taskRepository._tasks[0].name).toBe(name);
  });

  test('zero length name', async () => {
    const name = '';
    await expect(taskApplication.update(id, { name })).rejects.toThrow();
  });
  test('over length name', async () => {
    await expect(taskApplication.register('123456789012345678901')).rejects.toThrow();
  });
  test('duplication name', async () => {
    const name = 'task';
    await taskApplication.register(name);

    await expect(taskApplication.update(id, { name })).rejects.toThrow();
  });

  test('only order specified', async () => {
    const name = 'a';
    const order = 5;
    await taskApplication.update(id, { order });
    expect(taskRepository._tasks[0].name).toBe(name);
    expect(taskRepository._tasks[0].order).toBe(order);
  });

  test('minus order', async () => {
    const order = -5;
    await expect(taskApplication.update(id, { order })).rejects.toThrow();
  });

  test('min order', async () => {
    await taskApplication.register('task2');
    const { id: id2 } = taskRepository._tasks[1];
    const order = 0;
    await taskApplication.update(id2, { order });
    expect(taskRepository._tasks[0].order).toBe(order);
  });

  test('max order', async () => {
    const order = 9999;
    await taskApplication.update(id, { order });
    expect(taskRepository._tasks[0].order).toBe(order);
  });

  test('over max order', async () => {
    const order = 10000;
    await expect(taskApplication.update(id, { order })).rejects.toThrow();
  });

  test('not found id', async () => {
    const order = 0;
    await expect(taskApplication.update('notfound', { order })).rejects.toThrow();
  });
});

describe('task delete', () => {
  let id: string;
  beforeEach(async () => {
    await taskApplication.register('a');
    ({ id } = taskRepository._tasks[0]);
  });

  test('exists', async () => {
    await taskApplication.delete(id);
    expect(taskRepository._tasks[0]).toBeFalsy();
  });

  test('not exists', async () => {
    await taskApplication.delete('notexists');
  });
});
