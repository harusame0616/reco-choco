import { defineComponent } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const DefaultPage = styled.div`
  display: flex;
  height: 100%;
  width: 100vw;
  color: #fffffe;
`;
const MainMenu = styled.div`
  height: 100%;
  width: 200px;
  overflow-y: auto;
  padding: 20px;
  background: #303034;
`;

const MainContent = styled.div`
  width: 100%;
  height: 100%;
  padding: 20px;
  overflow-y: auto;
  background: #242629;
`;

export default defineComponent({
  setup() {
    return () => (
      <DefaultPage>
        <MainMenu>
          <domain-main-menu />
        </MainMenu>
        <MainContent>
          <nuxt></nuxt>
        </MainContent>
      </DefaultPage>
    );
  }
});
