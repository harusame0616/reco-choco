import { ipcMain } from 'electron';
import { TaskApplication } from '../domains/applications/task/task-application';
import { getRepository } from '../domains/infrastractures/repository';

export const taskApplication = new TaskApplication(getRepository('task'));

type TaskChangeHook = () => Promise<void>;
let taskChangeHook: TaskChangeHook | undefined;
export const setTaskChangeHook = async (f: TaskChangeHook) => {
  taskChangeHook = f;
};

ipcMain.handle('api-task-register', async (event, { name }) => {
  await taskApplication.register(name);
  await taskChangeHook?.();
});

ipcMain.handle('api-task-list', async () => {
  return await taskApplication.list();
});

ipcMain.handle('api-task-deletion', async (_, { id }) => {
  await taskApplication.delete(id);
  await taskChangeHook?.();
});

ipcMain.handle('api-task-update', async (_, { id, data }) => {
  await taskApplication.update(id, data);
  await taskChangeHook?.();
});
