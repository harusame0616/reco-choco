import dayjs from 'dayjs';
import { getDatastore } from './helper';

export class NedbChankQuery {
  private static _chunk = getDatastore('chunk');
  private static _task = getDatastore('task');

  constructor() {}

  async findById(id: string) {
    const chank = await NedbChankQuery._chunk.asyncFindOne<any>({
      _id: id
    });
    if (!chank) {
      return undefined;
    }

    const task = await NedbChankQuery._task.asyncFindOne<any>({
      _id: (chank as any).taskId
    });

    return {
      id: chank._id,
      beginTime: chank.beginTime,
      finishTime: chank.finishTime,
      taskId: chank.taskId,
      taskName: task?.name,
      comment: chank.comment
    };
  }

  async findLeatest() {
    const chank = await NedbChankQuery._chunk.asyncFindOne<any>({}, [['sort', { beginTime: -1 }]]);
    if (!chank) {
      return undefined;
    }

    const task = await NedbChankQuery._task.asyncFindOne<any>({
      _id: (chank as any).taskId
    });

    return {
      id: chank._id,
      beginTime: chank.beginTime,
      finishTime: chank.finishTime,
      taskId: chank.taskId,
      taskName: task?.name,
      comment: chank.comment
    };
  }

  async findByDate(date: Date) {
    const today = new Date(dayjs(date).format('YYYY/MM/DD'));
    const tomorrow = new Date(dayjs(date).add(1, 'day').format('YYYY/MM/DD'));
    const chanks = await NedbChankQuery._chunk.asyncFind({
      $and: [
        { beginTime: { $lt: tomorrow } },
        {
          $or: [
            {
              finishTime: { $gte: today }
            },
            {
              finishTime: null
            },
            { finishTime: { $exists: false } }
          ]
        }
      ]
    });
    const tasks = await NedbChankQuery._task.asyncFind({
      _id: { $in: chanks.map(({ taskId }: any) => taskId) }
    });

    const mappedTask = Object.fromEntries(tasks.map((task: any) => [task._id, task]));

    return chanks.map((chank: any) => ({
      id: chank._id,
      beginTime: chank.beginTime,
      finishTime: chank.finishTime,
      taskId: chank.taskId,
      taskName: mappedTask[chank.taskId]?.name,
      comment: chank.comment
    }));
  }
}
