import { defineComponent, inject, PropType } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';
import { useTask } from '~/composables/use-task';

const TaskList = styled.div`
  display: flex;
`;

const Task = styled.div`
  background: #242629;
  border-radius: 5px;
  padding: 5px;
`;

export default defineComponent({
  props: {
    injectionKey: {
      type: Symbol as PropType<Symbol>,
      required: true
    }
  },
  setup(props) {
    const { tasks, changeName, deleteTask, updateOrders } = inject(props.injectionKey) as ReturnType<typeof useTask>;

    return () => (
      <TaskList>
        <draggable list={tasks.value} vOn:end={updateOrders}>
          {tasks.value.map((task, i) => (
            <Task key={task.id}>
              <base-input value={task.name} input={(name: string) => changeName(i, name)} delay />
              <case-delete-button vOn:click={() => deleteTask(i)} />
            </Task>
          ))}
        </draggable>
      </TaskList>
    );
  }
});
