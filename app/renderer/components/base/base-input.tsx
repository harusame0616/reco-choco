import { watch, Ref, defineComponent, PropType, ref } from '@vue/composition-api';
import styled from 'vue-styled-components';

const StyledInput = styled.input`
  padding: 5px 10px;
  outline: none;
  background: inherit;
`;

export default defineComponent({
  components: {
    StyledInput
  },
  props: {
    value: {
      type: String as PropType<string>,
      required: true
    },
    input: {
      type: Function as PropType<(text: string) => void>,
      required: true
    },
    enter: {
      type: Function as PropType<(text: string) => void>,
      required: false,
      default: () => {}
    },
    delay: {
      type: Boolean as PropType<boolean>,
      required: false,
      default: false
    }
  },
  setup(props, { attrs, listeners }) {
    const work = ref('') as Ref<string>;

    watch(
      () => props,
      () => (work.value = props.value),
      { immediate: true, deep: true }
    );

    const keyBoardTrigger = (keyboardEvent: KeyboardEvent) => {
      if (keyboardEvent.code === 'Enter') {
        const text = work.value;
        work.value = '';
        return props.input(text);
      }
    };

    const input = (text: string) => {
      if (!props.delay) {
        return props.input(text);
      }
      work.value = text;
    };

    return () => (
      <StyledInput value={work.value} vOn:input={input} vOn:keyup={keyBoardTrigger} {...{ attrs, on: listeners }} />
    );
  }
});
