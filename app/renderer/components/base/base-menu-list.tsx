import { defineComponent, PropType } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const MenuList = styled.div``;

const Link = styled.div`
  border-radius: 2px;
  border-bottom: #94a1b2 1px solid;

  &:hover {
    background: #7f5af0;
  }

  a {
    display: block;
    width: 100%;
    padding: 10px;
  }
`;

export interface Menu {
  label: string;
  to: string;
}

export default defineComponent({
  props: {
    menus: {
      type: Array as PropType<Menu[]>,
      required: true
    }
  },
  setup(props) {
    return () => (
      <MenuList>
        {props.menus.map(({ label, to }) => (
          <Link>
            <nuxt-link to={to}>{label}</nuxt-link>
          </Link>
        ))}
      </MenuList>
    );
  }
});
