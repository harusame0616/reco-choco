import { defineComponent } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const Card = styled.div`
  border-radius: 5px;
  padding: 15px;
  background: #16161a;
`;

export default defineComponent({
  setup(props, { slots }) {
    return () => <Card>{slots.default?.()}</Card>;
  }
});
