import { defineComponent, InjectionKey, provide } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';
import { useChankAggregation } from '~/composables/use-chank-aggregation';

export default defineComponent({
  setup() {
    const chankAggregation = useChankAggregation();
    const injectionKey: InjectionKey<typeof chankAggregation> = Symbol('ChankAggregationKey');
    provide(injectionKey, chankAggregation);

    return () => (
      <template-page>
        <base-date-control injectionKey={injectionKey} />
        <domain-task-aggregation-list injectionKey={injectionKey} />
      </template-page>
    );
  }
});
