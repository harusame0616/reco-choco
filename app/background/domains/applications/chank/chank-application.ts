import { Chank, ModifyData } from '../../models/chank/chank';
import { TaskRepositoryInterface } from '../task/task-repository-interface';
import { ChankDomainService } from './chank-domain-service';
import { ChankQueryInterface } from './chank-query-interface';
import { ChankRepositoryInterface } from './chank-repository-interface';

export interface ChankDto {
  id: string;
  taskId: string;
  beginTime: Date;
  finishTime: Date;
  taskName: string;
}

export const chankToDto = (chank: Chank) => {
  return { id: chank.id, beginTime: chank.beginTime, finishTime: chank.finishTime, taskId: chank.taskId };
};

export class ChankApplication {
  private _domainService: ChankDomainService;

  constructor(
    _taskRepository: TaskRepositoryInterface,
    private _chankRepository: ChankRepositoryInterface,
    private _chankQuery: ChankQueryInterface
  ) {
    this._domainService = new ChankDomainService(_taskRepository, _chankRepository);
  }

  async start(taskId: string) {
    const newChank = Chank.create(taskId);

    await this._domainService.canStart(newChank);
    await this._chankRepository.insert(newChank);
    return newChank.id;
  }

  async finish(chankId: string) {
    const chank = await this._chankRepository.findOneById(chankId);
    if (!chank) {
      throw new Error('チャンクが見つかりません');
    }

    if (chank.isFinished) {
      throw new Error('終了済みのチャンクです');
    }

    chank.finish();
    this._chankRepository.update(chank);
  }

  async modify(id: string, modifyData: ModifyData): Promise<void> {
    const chank = await this._chankRepository.findOneById(id);
    if (!chank) {
      throw new Error('チャンクが見つかりません');
    }

    chank.modify(modifyData);
    await this._domainService.canModify(chank);
    await this._chankRepository.update(chank);
  }

  async delete(id: string) {
    await this._chankRepository.deleteById(id);
  }

  async listByDate(date: Date) {
    return await this._chankQuery.findByDate(date);
  }

  async getActiveChank() {
    const chank = await this._chankQuery.findLeatest();
    return chank?.finishTime ? undefined : chank;
  }

  async getDetail(id: string) {
    return await this._chankQuery.findById(id);
  }
}
