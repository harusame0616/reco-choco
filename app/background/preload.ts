import { contextBridge, ipcRenderer } from 'electron';
import { ModifyData } from './domains/models/chank/chank';

const api = {
  taskApi: {
    register: async (name: string) => await ipcRenderer.invoke('api-task-register', { name }),
    list: async () => await ipcRenderer.invoke('api-task-list'),
    delete: async (id: string) => await ipcRenderer.invoke('api-task-deletion', { id }),
    update: async (id: string, data: { name?: string; order?: number }) =>
      await ipcRenderer.invoke('api-task-update', { id, data })
  },
  chankApi: {
    start: async (id: string) => await ipcRenderer.invoke('api-chank-start', { id }),
    finish: async (id: string) => await ipcRenderer.invoke('api-chank-finish', { id }),
    modify: async (id: string, data: ModifyData) => await ipcRenderer.invoke('api-chank-modify', { id, data }),
    listByDate: async (date: Date) => await ipcRenderer.invoke('api-chank-list-by-date', { date }),
    delete: async (id: string) => await ipcRenderer.invoke('api-chank-delete', { id }),
    getDetail: async (id: string) => await ipcRenderer.invoke('api-chank-get-detail', { id }),
    getActiveChank: async () => await ipcRenderer.invoke('api-chank-get-active-chank', {})
  }
};

export type Api = typeof api;

contextBridge.exposeInMainWorld('api', api);
