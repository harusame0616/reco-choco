import { defineComponent } from '@vue/composition-api';
import styled from 'vue-styled-components';

const PageTemplate = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const PageContent = styled.div`
  width: 100%;
  max-width: 680px;
`;

export default defineComponent({
  setup(_, { slots }) {
    return () => (
      <PageTemplate>
        <PageContent>{slots.default?.()}</PageContent>
      </PageTemplate>
    );
  }
});
