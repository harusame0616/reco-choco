import { defineComponent, PropType } from '@nuxtjs/composition-api';
import { Menu } from '../base/base-menu-list';

export default defineComponent({
  setup() {
    const menuList: Menu[] = [
      { label: 'ステータス', to: '/chank' },
      { label: '集計', to: '/aggregation' },
      { label: '設定', to: '/task' }
    ];

    return () => <base-menu-list menus={menuList} />;
  }
});
