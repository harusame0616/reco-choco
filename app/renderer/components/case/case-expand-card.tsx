import { defineComponent, ref } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const ExpandCard = styled.div``;

const ExpandTitle = styled.div`
  cursor: pointer;
`;

export default defineComponent({
  setup(_, { slots }) {
    let isExpand = ref(false);

    return () => (
      <ExpandCard>
        <base-card>
          <ExpandTitle vOn:click={() => (isExpand.value = !isExpand.value)}>
            <slot name="default">{slots.default?.()}</slot>
          </ExpandTitle>
          {isExpand.value ? (
            <div>
              <slot name="content">{slots.content?.()}</slot>
            </div>
          ) : undefined}
        </base-card>
      </ExpandCard>
    );
  }
});
