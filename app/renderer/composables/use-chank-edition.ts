import {
  computed,
  onMounted,
  onUnmounted,
  readonly,
  Ref,
  ref,
  useAsync,
  useContext,
  useFetch
} from '@nuxtjs/composition-api';
import dayjs from 'dayjs';

export const useChankEdition = (chankId: string | undefined) => {
  let chank = ref({}) as Ref<any>;
  let beginTime = ref('') as Ref<string>;
  let finishTime = ref('') as Ref<string>;
  let taskId = ref('') as Ref<string | undefined>;
  let comment = ref('') as Ref<string>;
  let elappsedTime = ref(0) as Ref<number>;
  let tasks = ref([]) as Ref<string[]>;

  const {
    $api: { chankApi, taskApi }
  } = useContext();

  useAsync(async () => {
    tasks.value = await taskApi.list();
  });

  const { fetch } = useFetch(async () => {
    chank.value = await (chankId ? chankApi.getDetail(chankId) : chankApi.getActiveChank());
    if (!chank.value) {
      chank.value = {};
      beginTime.value = '';
      finishTime.value = '';
      taskId.value = undefined;
      comment.value = '';
      return;
    }
    chankId = chank.value.id;
    taskId.value = chank.value.taskId;

    beginTime.value = dayjs(chank.value.beginTime).format('YYYY-MM-DDTHH:mm');
    if (chank.value.finishTime) {
      finishTime.value = dayjs(chank.value.finishTime).format('YYYY-MM-DDTHH:mm');
    }
    comment.value = chank.value.comment ?? '';
    updateElapsed();
  });
  fetch();
  const changeFinishTime = (dateTime: string) => {
    finishTime.value = dateTime;
  };

  const changeBeginTime = (dateTime: string) => {
    beginTime.value = dateTime;
  };

  const changeComment = (text: string) => {
    comment.value = text;
  };

  const changeTask = (id: string) => {
    taskId.value = id;
  };

  const isStarted = computed(() => !!beginTime.value);
  const isFinished = computed(() => !!finishTime.value);

  const isChanged = computed(() => {
    console.log(isStarted.value);
    if (!isStarted.value) {
      return false;
    }

    if (!dayjs(chank.value?.beginTime).isSame(beginTime?.value)) {
      return true;
    }

    if (
      !dayjs(chank.value?.finishTime).isSame(finishTime?.value) &&
      !chank.value?.finishTime &&
      finishTime.value != ''
    ) {
      return true;
    }
    if ((chank.value.comment ?? '') !== comment.value) {
      return true;
    }
    if ((chank.value.taskId ?? '') !== taskId.value) {
      return true;
    }
    return false;
  });

  const canStart = computed(() => {
    return taskId.value != null && !isStarted.value;
  });

  const canFinish = computed(() => {
    return taskId.value != null && isStarted.value && !isFinished.value;
  });

  const start = async () => {
    if (!taskId.value) {
      throw new Error('error: required task id');
    }
    await chankApi.start(taskId.value);
    fetch();
  };

  const finishChank = async () => {
    if (!chankId) {
      return new Error('記録が開始されていません');
    }

    if (isChanged.value) {
      await save();
    }

    await chankApi.finish(chankId);
    fetch();
  };

  const save = async () => {
    if (!chankId) {
      return new Error('記録が開始されていません');
    }

    await chankApi.modify(chankId, {
      taskId: taskId.value,
      beginTime: beginTime.value ? new Date(beginTime.value) : undefined,
      finishTime: finishTime.value ? new Date(finishTime.value) : undefined,
      comment: comment.value
    });
    fetch();
  };
  const reset = () => {
    fetch();
  };
  const updateElapsed = () => {
    if (!beginTime.value) {
      return;
    }
    if (finishTime.value) {
      elappsedTime.value = dayjs(finishTime.value).diff(beginTime.value, 'minute');
    } else {
      elappsedTime.value = dayjs().diff(beginTime.value, 'minute');
    }
  };
  let intervalId: ReturnType<typeof setInterval> | null = null;
  onMounted(() => (intervalId = setInterval(updateElapsed, 1000)));

  onUnmounted(() => intervalId && clearInterval(intervalId));

  return {
    changeTask,
    isStarted,
    isChanged,
    canStart,
    canFinish,
    start,
    finishChank,
    save,
    reset,
    changeBeginTime,
    changeFinishTime,
    changeComment,
    taskId: readonly(taskId),
    beginTime: readonly(beginTime),
    finishTime: readonly(finishTime),
    elappsedTime: readonly(elappsedTime),
    comment: readonly(comment)
  };
};
