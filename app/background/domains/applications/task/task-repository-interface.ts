import { Task } from '../../models/task/task';

export interface TaskRepositoryInterface {
  findOneByLastOrder(): Promise<Task | undefined>;
  findOneById(id: string): Promise<Task | undefined>;
  findOneByName(name: string): Promise<Task | undefined>;
  insert(task: Task): Promise<void>;
  list(): Promise<Task[]>;
  update(task: Task): Promise<void>;
  deleteById(id: string): Promise<void>;
}
