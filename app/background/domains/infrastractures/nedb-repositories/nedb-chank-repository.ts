import { ChankRepositoryInterface } from '../../applications/chank/chank-repository-interface';
import { Chank } from '../../models/chank/chank';
import { getDatastore } from './helper';

export interface ChankEntity {
  _id: string;
  taskId: string;
  beginTime: Date;
  finishTime: Date;
  taskName: string;
  comment: string;
}

export const chankFromDBEntity = (chankDto?: ChankEntity | null) => {
  return chankDto
    ? new Chank(chankDto._id, chankDto.taskId, chankDto.beginTime, chankDto.finishTime, chankDto.comment)
    : undefined;
};

export class NedbChankRepository implements ChankRepositoryInterface {
  private static _db = getDatastore('chunk');

  constructor() {}

  async findOneById(id: string) {
    return chankFromDBEntity(await NedbChankRepository._db.asyncFindOne({ _id: id }));
  }

  async findByTaskId(taskId: string) {
    return (await NedbChankRepository._db.asyncFind({ taskId })).map(
      (chankEntity: any) => chankFromDBEntity(chankEntity) as Chank
    );
  }

  async list() {
    return (await NedbChankRepository._db.asyncFind({})).map(
      (chankEntity: any) => chankFromDBEntity(chankEntity) as Chank
    );
  }

  async insert(chank: Chank) {
    await NedbChankRepository._db.asyncInsert({
      _id: chank.id,
      taskId: chank.taskId,
      beginTime: chank.beginTime,
      finishTime: chank.finishTime,
      comment: chank.comment
    });
  }

  async update(chank: Chank) {
    await NedbChankRepository._db.asyncUpdate(
      { _id: chank.id },
      { taskId: chank.taskId, beginTime: chank.beginTime, finishTime: chank.finishTime, comment: chank.comment }
    );
  }

  async deleteById(id: string) {
    await NedbChankRepository._db.asyncRemove({ _id: id });
  }
}
