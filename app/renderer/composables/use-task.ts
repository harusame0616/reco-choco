import { readonly, Ref, ref, useContext, useFetch } from '@nuxtjs/composition-api';
import { TaskApplication } from '../plugins/api';

const getError = (err: any) => {
  return (err as Error).message?.split?.(':')?.slice?.(-1)?.[0];
};

export const useTask = () => {
  const tasks = ref([]) as Ref<TaskApplication.TaskDTO[]>;
  const newTaskName = ref('') as Ref<string>;
  const error = ref('') as Ref<string>;

  const {
    $api: { taskApi }
  } = useContext();
  const { fetch } = useFetch(async () => {
    tasks.value = await taskApi.list();
  });

  const register = async (name: string) => {
    resetError();

    try {
      await taskApi.register(name);
    } catch (err: any) {
      error.value = getError(err);
    }

    newTaskName.value = '';
    fetch();
  };

  const changeName = async (index: number, name: string) => {
    resetError();

    const task = tasks.value[index];
    if (!task || !task.id) {
      return new Error('error');
    }

    try {
      await taskApi.update(task.id, { name });
    } catch (err) {
      error.value = getError(err);
    }
    fetch();
  };

  const deleteTask = async (index: number) => {
    resetError();

    const task = tasks.value[index];
    if (task && task.id) {
      try {
        await taskApi.delete(task.id);
      } catch (err) {
        error.value = getError(err);
      }
    }
    fetch();
  };

  const updateOrders = async () => {
    tasks.value.forEach((task, i) => {
      task.order = i;
    });

    await Promise.all(tasks.value.map(({ id, order }) => taskApi.update(id, { order })));
    fetch();
  };

  const inputNewTaskName = (name: string) => {
    newTaskName.value = name;
  };

  const resetError = () => {
    error.value = '';
  };

  return {
    register,
    changeName,
    deleteTask,
    newTaskName,
    inputNewTaskName,
    updateOrders,
    tasks: readonly(tasks),
    error,
    resetError
  };
};
