import { useRoute } from '@nuxtjs/composition-api';
import { defineComponent } from '@vue/composition-api';
import styled from 'vue-styled-components';
import { useChankEdition } from '~/composables/use-chank-edition';
import { useTask } from '~/composables/use-task';

const TaskName = styled.div`
  font-size: 38pxt
  height: 48px;
  margin-bottom: 24px;
`;

const ControlWrap = styled.div`
  display: flex;
  justify-content: flex-end;
  gap: 20px;
  margin-top: 20px;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
  height: 28px;
`;

const Time = styled.div`
  min-width: 1rem;
  height: 1rem;

  &:empty {
    &:after {
      display: flex;
      justify-content: right;
      margin-right: 0.5rem;
      content: '-';
    }
  }
`;
const Comment = styled.textarea`
  width: 100%;
  padding: 24px;
  height: 200px;
  border: 1px solid grey;
  border-radius: 5px;
  resize: none;
  outline: none;
`;
const CommentWrap = styled.div`
  margin-top: 20px;
`;

export default defineComponent({
  setup() {
    const route = useRoute();

    const { tasks } = useTask();
    const chank = useChankEdition(route.value.params.id);

    return () => (
      <template-page>
        <TaskName>
          <base-select
            change={chank.changeTask}
            value={chank.taskId.value}
            options={tasks.value}
            value-name="id"
            label-name="name"
            placeholder="タスクを選択してください"
            key="test"
          />
        </TaskName>
        <Row>
          開始：
          <base-input
            type="datetime-local"
            value={chank.beginTime.value}
            input={chank.changeBeginTime}
            disabled={!chank.isStarted.value}
          />
          {!chank.isStarted.value && (
            <base-button vOn:click={chank.start} disabled={!chank.canStart.value}>
              start
            </base-button>
          )}
        </Row>
        <Row>
          終了：
          <base-input
            type="datetime-local"
            value={chank.finishTime.value}
            input={chank.changeFinishTime}
            disabled={!chank.isStarted.value}
          />
          {chank.canFinish.value ? <base-button vOn:click={chank.finishChank}>DONE</base-button> : undefined}
        </Row>
        <Row>
          時間： <Time>{chank.elappsedTime.value}</Time>分
        </Row>
        <CommentWrap>
          <Comment vOn:input={chank.changeComment} disabled={!chank.isStarted.value} value={chank.comment.value} />
        </CommentWrap>
        <ControlWrap>
          <base-button disabled={!chank.isChanged.value} vOn:click={chank.reset}>
            CANCEL
          </base-button>
          <base-button disabled={!chank.isChanged.value} vOn:click={chank.save}>
            SAVE
          </base-button>
        </ControlWrap>
      </template-page>
    );
  }
});
