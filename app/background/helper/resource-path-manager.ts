import { PathConverter } from './path-converter';
import path from 'path';

const paths = {
  tryIcon: 'public/tray/icon.png'
};

type ResourceName = keyof typeof paths;

export class ResourcePathManager {
  constructor(private pathConverter: PathConverter) {}

  getPath(name: ResourceName): string {
    return this.pathConverter.convert(paths[name]);
  }
}

export class PrdResourcePathConverter implements PathConverter {
  convert(resourcePath: string): string {
    return path.resolve(process.resourcesPath, resourcePath);
  }
}

export class DevResourcePathConverter implements PathConverter {
  convert(resourcePath: string): string {
    return path.resolve('./', resourcePath);
  }
}
