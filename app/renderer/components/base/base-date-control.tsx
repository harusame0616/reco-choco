import { defineComponent, inject, PropType, Ref } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const DateControl = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const DateArea = styled.div`
  margin-bottom: 10px;
`;
const Year = styled.div`
  display: flex;
  justify-content: center;
  font-size: 16px;
`;
const Date = styled.div`
  font-size: 32px;
`;
const ControlArea = styled.div`
  display: flex;
  height: 32px;
`;
const Control = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 30px;
  height: 30px;
  border-radius: 5px;
  margin: 0 5px;
  cursor: pointer;
  user-select: none;

  &:active {
    margin-top: 1px;
  }
  &:hover {
    background: #7f5af0;
  }
`;
const WideControl = Control.extend`
  width: 60px;
`;

export interface DateControl {
  prevMonth(): void;
  prevDay(): void;
  nextMonth(): void;
  nextDay(): void;
  toToday(): void;
  date: Ref<{ year: number; month: number; day: number }>;
}

export default defineComponent({
  props: {
    injectionKey: {
      type: Symbol as PropType<Symbol>,
      required: true
    }
  },
  setup(props) {
    const { prevMonth, prevDay, nextDay, nextMonth, date, toToday } = inject(props.injectionKey) as DateControl;

    return () => (
      <DateControl>
        <DateArea>
          <Year>{date.value.year}</Year>
          <Date>
            {date.value.month}月{date.value.day}日
          </Date>
        </DateArea>
        <ControlArea>
          <Control vOn:click={() => prevMonth()}>
            <fa-icon icon={['fas', 'caret-left']} />
            <fa-icon icon={['fas', 'caret-left']} />
          </Control>
          <Control vOn:click={() => prevDay()}>
            <fa-icon icon={['fas', 'caret-left']} />
          </Control>
          <WideControl vOn:click={() => toToday()}>今日</WideControl>
          <Control vOn:click={() => nextDay()}>
            <fa-icon icon={['fas', 'caret-right']} />
          </Control>
          <Control vOn:click={() => nextMonth()}>
            <fa-icon icon={['fas', 'caret-right']} />
            <fa-icon icon={['fas', 'caret-right']} />
          </Control>
        </ControlArea>
      </DateControl>
    );
  }
});
