import { defineComponent, inject, PropType } from '@vue/composition-api';
import { useTask } from '~/composables/use-task';

export default defineComponent({
  props: {
    injectionKey: {
      type: Symbol as PropType<Symbol>,
      required: true
    }
  },
  setup(props) {
    const { newTaskName, register } = inject(props.injectionKey) as ReturnType<typeof useTask>;
    return () => <base-input placeholder="タスクの追加" value={newTaskName.value} input={register} delay />;
  }
});
