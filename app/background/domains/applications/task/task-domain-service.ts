import { Task } from '../../models/task/task';
import { TaskRepositoryInterface } from './task-repository-interface';

export class TaskDomainService {
  constructor(private _taskRepository: TaskRepositoryInterface) {}
  async canRegister(task: Task) {
    const [taskById, taskByName] = await Promise.all([
      this._taskRepository.findOneById(task.id),
      this._taskRepository.findOneByName(task.name)
    ]);

    if (taskById) {
      throw new Error('登録済みのタスクIDです');
    }
    if (taskByName) {
      throw new Error('登録済みのタスク名前です');
    }
  }

  async canUpdate(task: Task) {
    const taskByName = await this._taskRepository.findOneByName(task.name);

    if (taskByName && !taskByName.equals(task)) {
      throw new Error('登録済みのタスク名前です');
    }
  }
}
