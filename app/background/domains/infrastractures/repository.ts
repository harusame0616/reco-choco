import { ChankQueryInterface } from '../applications/chank/chank-query-interface';
import { ChankRepositoryInterface } from '../applications/chank/chank-repository-interface';
import { TaskRepositoryInterface } from '../applications/task/task-repository-interface';
import { NedbChankQuery } from './nedb-repositories/nedb-chank-query';
import { NedbChankRepository } from './nedb-repositories/nedb-chank-repository';
import { NedbTaskRepository } from './nedb-repositories/nedb-task-repository';
import { OMChankQuery } from './on-memory-repositories/om-chank-query';
import { OMChankRepository } from './on-memory-repositories/om-chank-repository';
import { OMTaskRepository } from './on-memory-repositories/om-task-repository';

interface Repository {
  task: TaskRepositoryInterface;
  chank: ChankRepositoryInterface;
  chankQuery: ChankQueryInterface;
}

const repositoryTypes = ['om', 'nedb'] as const;
const isRepositoryType = (arg: any): arg is RepositoryType => {
  return repositoryTypes.includes(arg);
};
type RepositoryType = typeof repositoryTypes[number];

const createRepository: (type: RepositoryType) => Repository = (type) => {
  if (type === 'om') {
    return {
      task: new OMTaskRepository(),
      chank: new OMChankRepository(),
      chankQuery: new OMChankQuery()
    };
  } else if (type === 'nedb') {
    return {
      task: new NedbTaskRepository(),
      chank: new NedbChankRepository(),
      chankQuery: new NedbChankQuery()
    };
  } else {
    throw new Error('not found repository type: ' + type);
  }
};

const repositoryType = isRepositoryType(process.env.REPOSITORY_TYPE) ? process.env.REPOSITORY_TYPE : 'nedb';
const repository = createRepository(repositoryType);

export const getRepository = <T extends keyof Repository, K extends Repository[T]>(name: T): K => {
  return repository[name] as K;
};
