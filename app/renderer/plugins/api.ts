import { defineNuxtPlugin } from '@nuxtjs/composition-api';
import { Api } from '../../background/preload';
import * as TaskApplication from '../../background/domains/applications/task/task-application';
import * as ChankApplication from '../../background/domains/applications/chank/chank-application';
export { TaskApplication, ChankApplication };

declare module 'vue/types/vue' {
  // this.$myInjectedFunction inside Vue components
  interface Vue {
    $api: Api;
  }
}

declare module '@nuxt/types' {
  // nuxtContext.app.$myInjectedFunction inside asyncData, fetch, plugins, middleware, nuxtServerInit
  interface NuxtAppOptions {
    $api: Api;
  }
  // nuxtContext.$myInjectedFunction
  interface Context {
    $api: Api;
  }
}

declare module 'vuex/types/index' {
  // this.$myInjectedFunction inside Vuex stores
  interface Store<S> {
    $api: Api;
  }
}

export default defineNuxtPlugin((context, inject) => {
  inject('api', (window as any).api);
});
