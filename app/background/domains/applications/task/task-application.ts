import { Task } from '../../models/task/task';
import { TaskDomainService } from './task-domain-service';
import { TaskRepositoryInterface } from './task-repository-interface';

export interface TaskDTO {
  name: string;
  id: string;
  order: number;
}

export const taskToDto = (task: Task) => {
  return { name: task.name, id: task.id, order: task.order };
};

export class TaskApplication {
  private _domainService: TaskDomainService;

  constructor(private _taskRepository: TaskRepositoryInterface) {
    this._domainService = new TaskDomainService(_taskRepository);
  }

  async register(name: string) {
    const task = await this._taskRepository.findOneByLastOrder();
    const newTask = Task.create(name, task?.order == undefined ? 0 : task.order + 1);

    await this._domainService.canRegister(newTask);
    await this._taskRepository.insert(newTask);
  }

  async list(): Promise<TaskDTO[]> {
    const x = (await this._taskRepository.list()).map(taskToDto).sort((a, b) => {
      if (a.order == b.order) {
        return 0;
      }
      return a.order < b.order ? -1 : 1;
    });

    return x;
  }

  async update(id: string, { name, order }: { name?: string; order?: number }): Promise<void> {
    const task = await this._taskRepository.findOneById(id);
    if (!task) {
      throw new Error('タスクが見つかりません');
    }

    if (name != undefined) {
      task.name = name;
    }
    if (order != undefined) {
      task.order = order;
    }

    await this._domainService.canUpdate(task);
    await this._taskRepository.update(task);
  }

  async delete(id: string) {
    await this._taskRepository.deleteById(id);
  }
}
