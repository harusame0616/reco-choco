import { defineComponent, InjectionKey, provide, watch } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';
import { useTask } from '~/composables/use-task';

const TaskPage = styled.div`
  position: relative;
`;

const ErrorMessage = styled.div`
  position: fixed;
  bottom: 0;
  left: 0px;
  right: 0px;
  padding: 10px;
  font-size: 12px;
  background-color: #ff4e2a;
  transition: all 200ms ease-out;
  height: 32px;
  opacity: 100%;

  &:empty {
    transition: all 1s ease-out;
    height: 0;
    opacity: 0;
    padding: 0;
  }
`;
export default defineComponent({
  setup() {
    const task = useTask();
    const taskInjectionKey: InjectionKey<typeof task> = Symbol('taskInjectionKey');
    provide(taskInjectionKey, task);
    let timeoutId: any;

    watch(task.error, () => {
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => {
        task.resetError();
      }, 5000);
    });
    return () => (
      <TaskPage>
        <ErrorMessage>{task.error.value}</ErrorMessage>
        <domain-task-list injection-key={taskInjectionKey} />
        <domain-task-addition-form injection-key={taskInjectionKey} />
      </TaskPage>
    );
  }
});
