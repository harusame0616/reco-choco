import { defineComponent, PropType } from '@nuxtjs/composition-api';
import dayjs from 'dayjs';
import styled from 'vue-styled-components';
import { AggregatedTask } from '~/composables/use-chank-aggregation';

const TaskAggregationCard = styled.div``;

const TaskInfo = styled.div`
  display: flex;
  padding: 10px;
`;
const TaskName = styled.div`
  flex-grow: 1;
  font-size: 28px;
`;
const TaskTotalTime = styled.div`
  display: flex;
  align-items: flex-end;
  font-size: 24px;
`;
const Unit = styled.div`
  display: flex;
  align-items: flex-end;
  height: 100%;
  font-size: 12px;
  padding-bottom: 3px;
`;

const ChankList = styled.div`
  border-top: 1px solid #fffffe;
  padding: 10px;
`;
const ChankInfo = styled.div`
  display: flex;
  flex-grow: 1;
`;
const ChankAction = styled.div`
  display: flex;
  column-gap: 5px;
`;
const Chank = styled.div`
  display: flex;
  align-items: center;
  margin: 10px;
`;
const Datetime = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 160px;
  margin-right: 20px;
`;
const Date = styled.span`
  font-size: 12px;
  height: 15px;
`;
const Time = styled.span`
  font-size: 22px;
  margin-top: 2px;
`;
const ChankTime = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  font-size: 22px;
  width: 40px;
`;
const ChankTimeUnit = styled.div`
  align-items: flex-end;
  font-size: 12px;
  margin-left: 5px;
  margin-bottom: 2px;
`;

type DeleteFunction = (id: string) => void;
type EditFunction = (id: string) => void;

export default defineComponent({
  props: {
    taskAggregation: {
      type: Object as PropType<AggregatedTask>,
      required: true
    },
    deleteChank: {
      type: Function as PropType<DeleteFunction>,
      required: true
    },
    editChank: {
      type: Function as PropType<EditFunction>,
      required: true
    }
  },
  setup(props) {
    return () => (
      <TaskAggregationCard>
        <case-expand-card>
          <template slot="default">
            <TaskInfo>
              <TaskName>{props.taskAggregation.taskName}</TaskName>
              <TaskTotalTime>
                {Math.floor(props.taskAggregation.time / 60)}
                <Unit>分</Unit>
              </TaskTotalTime>
            </TaskInfo>
          </template>
          <template slot="content">
            <ChankList>
              {props.taskAggregation.chanks.map(({ beginTime, finishTime, id }) => (
                <Chank>
                  <ChankInfo>
                    <Datetime>
                      <Date>{dayjs(beginTime).format('MM/DD')}</Date>
                      <Time>{dayjs(beginTime).format('HH:mm')}</Time>
                    </Datetime>
                    {finishTime ? (
                      <Datetime>
                        <Date>{dayjs(finishTime).format('MM/DD')}</Date>
                        <Time>{dayjs(finishTime).format('HH:mm')}</Time>
                      </Datetime>
                    ) : (
                      <Datetime>
                        <Date></Date>
                        <Time>-</Time>
                      </Datetime>
                    )}
                    <ChankTime>
                      {finishTime ? Math.floor(dayjs(finishTime).diff(beginTime, 'm')) : '-'}
                      <ChankTimeUnit>分</ChankTimeUnit>
                    </ChankTime>
                  </ChankInfo>
                  <ChankAction>
                    <case-edit-button vOn:click_stop={() => props.editChank(id)} />
                    <case-delete-button vOn:click_stop={() => props.deleteChank(id)} />
                  </ChankAction>
                </Chank>
              ))}
            </ChankList>
          </template>
        </case-expand-card>
      </TaskAggregationCard>
    );
  }
});
