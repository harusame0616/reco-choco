export interface PathConverter {
  convert(path: string): string;
}
