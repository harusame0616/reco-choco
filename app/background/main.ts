import { app, BrowserWindow, Menu, MenuItemConstructorOptions, nativeImage as NativeImage, Tray } from 'electron';
import { resolve } from 'path';
import { setTaskChangeHook, taskApplication } from './ipc-events/task';
import { chankApplication } from './ipc-events/chank';
import { DevPathConverter, PrdPathConverter, PathManager } from './helper/path-manager';
import {
  PrdResourcePathConverter,
  DevResourcePathConverter,
  ResourcePathManager
} from './helper/resource-path-manager';

const pathManager = new PathManager(
  process.env.NODE_ENV === 'development' ? new DevPathConverter() : new PrdPathConverter()
);
const resourcePathManager = new ResourcePathManager(
  process.env.NODE_ENV === 'development' ? new DevResourcePathConverter() : new PrdResourcePathConverter()
);

let tray: Tray | null = null;
const createTray = () => {
  tray = new Tray(NativeImage.createFromPath(resourcePathManager.getPath('tryIcon')));
  var contextMenu = Menu.buildFromTemplate([{ label: '終了', click: () => app.quit() }]);
  updateTrayMenu(tray);
  tray.setContextMenu(contextMenu);
};

interface ActiveChankMenu extends MenuItemConstructorOptions {
  chankId?: string;
}
let activeChankMenu: ActiveChankMenu = { label: '', visible: false };
let chankId: string | undefined;
const updateTrayMenu = async (tray: Tray) => {
  const taskList = await taskApplication.list();

  var contextMenu = Menu.buildFromTemplate([
    ...taskList.map((task) => ({
      label: task.name,
      click: async () => {
        if (chankId) {
          await chankApplication.finish(chankId);
          chankId = undefined;
        }
        chankId = await chankApplication.start(task.id);
        activeChankMenu = {
          label: task.name,
          visible: true,
          click: async () => {
            if (chankId) {
              await chankApplication.finish(chankId);
              chankId = undefined;
            }
            activeChankMenu.visible = false;
            updateTrayMenu(tray);
          }
        };
        updateTrayMenu(tray);
      }
    })),
    { type: 'separator' },
    activeChankMenu,
    { type: 'separator' },
    {
      label: 'タスク設定',
      click: async () => {
        const windows = BrowserWindow.getAllWindows();
        if (windows.length === 0) {
          await createWindow();
        } else {
          windows[0].show();
        }

        windows[0].loadURL(pathManager.getPath('taskSetting'));
      }
    },
    { label: '終了', click: () => app.quit() }
  ]);
  tray.setContextMenu(contextMenu);
};

setTaskChangeHook(async () => {
  if (!tray) {
    return;
  }
  updateTrayMenu(tray);
});

const createWindow = async () => {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: resolve(__dirname, 'preload.js')
    }
  });

  if (process.env.NODE_ENV === 'development') {
    mainWindow.webContents.openDevTools();
  }

  while (true) {
    try {
      await mainWindow.loadURL(pathManager.getPath('taskSetting'));
      break;
    } catch (e) {
      console.error(e);
      await new Promise((resolve) => setTimeout(resolve, 5000));
    }
  }
};

app.on('ready', async () => {
  if (process.env.NODE_ENV === 'development') {
    await createWindow();
  }
  await createTray();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', async () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    await createWindow();
  }
});
