import { defineComponent } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const EditButton = styled.button`
  display: inline-flexbox;
  justify-content: center;
  align-items: center;
  width: 28px;
  height: 28px;
  border-radius: 50%;
  background-color: #44d700;
  color: white;
  font-size: 16px;
  font-weight: bold;
`;

export default defineComponent({
  setup(_, { listeners, attrs }) {
    return () => (
      <EditButton {...{ ...attrs, on: listeners }}>
        <fa-icon icon={['fas', 'pencil-alt']} />
      </EditButton>
    );
  }
});
