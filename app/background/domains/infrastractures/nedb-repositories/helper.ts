import Datastore from 'nedb-async';
import os from 'os';

const createPath = (dbName: string) => {
  return `${os.homedir()}/.choco-reco/${dbName}.db`;
};

const dataStores = {
  chunk: new Datastore({
    filename: createPath('chunk'),
    autoload: true
  }),
  task: new Datastore({
    filename: createPath('task'),
    autoload: true
  })
};

type DataStores = typeof dataStores;
type DataStoreName = keyof DataStores;

export const getDatastore = (name: DataStoreName): Datastore<unknown> => {
  return dataStores[name];
};
