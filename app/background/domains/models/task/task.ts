import { v4 } from 'uuid';

export class Task {
  constructor(private _id: string, private _name: string, private _order: number) {
    this.name = _name;
    this.order = _order;
  }

  get id() {
    return this._id;
  }

  get name() {
    return this._name;
  }

  set name(name: string) {
    const maxLength = 20;
    if (!name.length) {
      throw new Error('名前が未設定です');
    }
    if (name.length > maxLength) {
      throw new RangeError(`名前が${maxLength}を超えています`);
    }

    this._name = name;
  }

  get order() {
    return this._order;
  }

  set order(order: number) {
    const maxOrder = 9999;
    const minOrder = 0;
    if (order < minOrder) {
      throw new RangeError(`表示順が${minOrder}未満です`);
    }

    if (order > maxOrder) {
      throw new RangeError(`表示順が${maxOrder}を超えています`);
    }

    this._order = order;
  }

  equals(task: Task) {
    return task.id === this.id;
  }

  static create(task: Task, _: never): Task;
  static create(name: string, order: number): Task;
  static create(arg1: Task | string, arg2: number): Task {
    if (arg1 instanceof Task && arg2 === undefined) {
      return new Task(arg1.id, arg1.name, arg1.order);
    } else if (typeof arg1 === 'string') {
      return new Task(v4(), arg1, arg2);
    } else {
      throw new Error();
    }
  }
}
