import { computed, readonly, Ref, ref, useContext, useFetch, useRouter } from '@nuxtjs/composition-api';
import { ChankApplication } from '../plugins/api';
import dayjs, { Dayjs } from 'dayjs';
import { AggregatedTask } from './use-chank-aggregation';

export const useChank = () => {
  let chanks = ref([]) as Ref<ChankApplication.ChankDto[]>;
  let aggregationDate = ref(dayjs());

  const {
    $api: { chankApi }
  } = useContext();
  const router = useRouter();

  const { fetch } = useFetch(async () => (chanks.value = await chankApi.listByDate(aggregationDate.value.toDate())));

  const tasks = computed(() => {
    const tasks: AggregatedTask[] = [];
    chanks.value.reduce((prev: any, chank: ChankApplication.ChankDto) => {
      if (!prev[chank.taskId]) {
        prev[chank.taskId] = { taskId: chank.taskId, taskName: chank.taskName, time: 0, chanks: [] };
        tasks.push(prev[chank.taskId]);
      }

      prev[chank.taskId].chanks.push(chank);
      if (chank.finishTime) {
        prev[chank.taskId].time += dayjs(chank.finishTime).diff(chank.beginTime, 's');
      }

      return prev;
    }, {});

    return tasks;
  });

  const changeAggregationDate = (date: Dayjs) => {
    aggregationDate.value = date;
    fetch();
  };

  const nextDay = () => changeAggregationDate(aggregationDate.value.add(1, 'd'));
  const prevDay = () => changeAggregationDate(aggregationDate.value.add(-1, 'd'));
  const nextMonth = () => changeAggregationDate(aggregationDate.value.add(1, 'month'));
  const prevMonth = () => changeAggregationDate(aggregationDate.value.add(-1, 'month'));
  const toToday = () => changeAggregationDate(dayjs());

  const date = computed(() => {
    return {
      year: aggregationDate.value.year(),
      month: aggregationDate.value.month() + 1,
      day: aggregationDate.value.date()
    };
  });

  const editChank = async (id: string) => {
    router.push(`/chank/${id}`);
  };

  const deleteChank = async (id: string) => {
    await chankApi.delete(id);
    fetch();
  };

  return {
    tasks,
    date,
    nextDay,
    prevDay,
    nextMonth,
    prevMonth,
    toToday,
    deleteChank,
    editChank
  };
};
