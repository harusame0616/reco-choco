import { TaskRepositoryInterface } from '../../applications/task/task-repository-interface';
import { Task } from '../../models/task/task';
import { getDatastore } from './helper';

interface TaskEntity {
  _id: string;
  name: string;
  order: number;
}

export const taskFromEntity = (entity?: TaskEntity | undefined | null) => {
  return entity && entity._id ? new Task(entity._id, entity.name, entity.order) : undefined;
};

export class NedbTaskRepository implements TaskRepositoryInterface {
  private static _db = getDatastore('task');

  constructor() {}

  async findOneByLastOrder(): Promise<Task | undefined> {
    return taskFromEntity(
      (
        await NedbTaskRepository._db.asyncFind({}, [
          ['sort', { order: -1 }],
          ['limit', 1]
        ])
      )?.[0] as any
    );
  }

  async findOneById(id: string) {
    return taskFromEntity(await NedbTaskRepository._db.asyncFindOne({ _id: id }));
  }

  async findOneByName(name: string) {
    return taskFromEntity(await NedbTaskRepository._db.asyncFindOne({ name }));
  }

  async list() {
    return (await NedbTaskRepository._db.asyncFind({})).map((taskEntity: any) => taskFromEntity(taskEntity) as Task);
  }

  async insert(task: Task) {
    await NedbTaskRepository._db.asyncInsert({ name: task.name, order: task.order, _id: task.id });
  }

  async update(task: Task) {
    await NedbTaskRepository._db.asyncUpdate({ _id: task.id }, { name: task.name, order: task.order });
  }

  async deleteById(id: string) {
    await NedbTaskRepository._db.asyncRemove({ _id: id });
  }
}
