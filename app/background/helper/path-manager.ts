import { PathConverter } from './path-converter';

const paths = {
  taskSetting: 'task'
};
type pathName = keyof typeof paths;

export class PathManager {
  constructor(private converter: PathConverter) {}
  getPath(name: pathName) {
    return this.converter.convert(paths[name]);
  }
}

export class DevPathConverter implements PathConverter {
  convert(path: string): string {
    return `http://localhost:3000/${path}`;
  }
}

export class PrdPathConverter implements PathConverter {
  convert(path: string): string {
    return 'file://' + __dirname + `/../../renderer/index.html#/${path}`;
  }
}
