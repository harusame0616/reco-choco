import { ChankApplication } from '../../../../domains/applications/chank/chank-application';
import { OMChankQuery } from '../../../../domains/infrastractures/on-memory-repositories/om-chank-query';
import { OMChankRepository } from '../../../../domains/infrastractures/on-memory-repositories/om-chank-repository';
import { OMTaskRepository } from '../../../../domains/infrastractures/on-memory-repositories/om-task-repository';
import { Task } from '../../../../domains/models/task/task';

let taskRepository: OMTaskRepository;
let chankRepository: OMChankRepository;
let chankApplication: ChankApplication;
let taskId: string;
const beginTime = '2021-01-01 12:11';

beforeEach(() => {
  taskRepository = new OMTaskRepository([Task.create('sample', 0), Task.create('sample2', 1)]);
  taskId = taskRepository._tasks[0].id;
  chankRepository = new OMChankRepository();
  chankApplication = new ChankApplication(taskRepository, chankRepository, new OMChankQuery());

  jest.useFakeTimers('modern');
  jest.setSystemTime(new Date(beginTime).getTime());
});

afterEach(() => {
  jest.useRealTimers();
});
describe('chank start', () => {
  test('normal start', async () => {
    await chankApplication.start(taskId);
    expect(chankRepository._chanks[0].beginTime.getTime()).toBe(new Date(beginTime).getTime());
    expect(chankRepository._chanks[0].finishTime).toBeUndefined();
    expect(chankRepository._chanks[0].taskId).toBe(taskId);
  });
  test('not exists task', async () => {
    await expect(chankApplication.start('not-exists')).rejects.toThrow();
  });
});

describe('chank finish', () => {
  const finishTime = '2021-01-01 19:00';
  let startedId: string;
  beforeEach(async () => {
    await chankApplication.start(taskId);
    startedId = chankRepository._chanks[0].id;
  });

  test('normal finish', async () => {
    jest.setSystemTime(new Date(finishTime).getTime());
    await chankApplication.finish(startedId);
    expect(chankRepository._chanks[0]?.finishTime?.getTime()).toBe(new Date(finishTime).getTime());
  });
  test('not exists chank', async () => {
    expect(chankApplication.finish('chank-id')).rejects.toThrow();
  });

  test('aleady finish', async () => {
    jest.setSystemTime(new Date(finishTime).getTime());
    await chankApplication.finish(startedId);

    expect(chankApplication.finish(startedId)).rejects.toThrow();
    expect(chankRepository._chanks[0]?.finishTime?.getTime()).toBe(new Date(finishTime).getTime());
  });
});

describe('chank modify', () => {
  let startedId: string;
  beforeEach(async () => {
    await chankApplication.start(taskId);
    startedId = chankRepository._chanks[0].id;
  });
  test('full specified', async () => {
    const newTaskId = taskRepository._tasks[1].id;

    const newBeginTime = '2021-01-01 13:11';
    const newFinishTime = '2021-01-01 14:11';
    await chankApplication.modify(startedId, {
      taskId: newTaskId,
      beginTime: new Date(newBeginTime),
      finishTime: new Date(newFinishTime)
    });

    expect(chankRepository._chanks[0].taskId).toBe(newTaskId);
    expect(chankRepository._chanks[0]?.finishTime?.getTime()).toBe(new Date(newFinishTime).getTime());
    expect(chankRepository._chanks[0]?.beginTime?.getTime()).toBe(new Date(newBeginTime).getTime());
  });

  test('not exists chank id', async () => {
    await expect(chankApplication.modify('not-exists', {})).rejects.toThrow();
  });

  test('not exists task id', async () => {
    await expect(chankApplication.modify(taskId, { taskId: 'not-exists' })).rejects.toThrow();
  });

  test('modify begin time to after finish time', async () => {
    const finishTime = '2021-01-01 14:11';
    jest.setSystemTime(new Date(finishTime).getTime());
    await chankApplication.finish(startedId);
    expect(
      chankApplication.modify(startedId, {
        beginTime: new Date('2021-01-01 14:12')
      })
    ).rejects.toThrow();
  });

  test('modify finish time to before begin time', async () => {
    const finishTime = '2021-01-01 14:11';
    jest.setSystemTime(new Date(finishTime).getTime());
    await chankApplication.finish(startedId);

    await expect(
      chankApplication.modify(startedId, {
        finishTime: new Date('2021-01-01 12:10')
      })
    ).rejects.toThrow();
  });
});
