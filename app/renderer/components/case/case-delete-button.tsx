import { defineComponent, SetupContext } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const DeleteButton = styled.button`
  display: inline-flexbox;
  justify-content: center;
  align-items: center;
  width: 28px;
  height: 28px;
  border-radius: 50%;
  background-color: #ff4e2a;
  color: white;
  font-size: 20px;
  font-weight: bold;
`;

export default defineComponent({
  components: {
    DeleteButton
  },
  setup(_, { listeners, attrs }) {
    return () => (
      <delete-button {...{ ...attrs, on: listeners }}>
        <fa-icon icon={['fas', 'times']} />
      </delete-button>
    );
  }
});
