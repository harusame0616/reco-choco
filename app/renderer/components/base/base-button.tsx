import { defineComponent } from '@vue/composition-api';
import styled from 'vue-styled-components';

const Button = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
  cursor: pointer;
  user-select: none;
  padding: 5px 0.75rem;
  width: 6rem;

  border: solid 1px #7f5af0;
  &:hover {
    background: #7f5af0;
  }
  &:disabled {
    opacity: 0.15;
    pointer-events: none;
  }
`;
export default defineComponent({
  setup(_, { slots, attrs, listeners }) {
    return () => <Button {...{ ...attrs, on: listeners }}>{slots?.default?.()}</Button>;
  }
});
