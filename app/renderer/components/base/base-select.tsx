import { computed, defineComponent, PropType } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';

const BaseSelect = styled.div`
  position: relative;
  outline: none;
  cursor: pointer;
  border-bottom: solid grey 1px;
  border-radius: 5px 5px 0 0
  padding: 5px 1rem;
  width: 100%;
`;

const Selector = styled.select`
  outline: none;
  width: 100%;
`;

const Placeholder = styled.div`
  position: absolute;
  width: 100%;
  pointer-events: none;
  opacity: 0.3;
`;

export default defineComponent({
  props: {
    change: {
      type: Function as PropType<(value: any) => void>,
      required: false
    },
    value: {
      type: [String, Number, null] as PropType<String | Number | null>,
      required: false,
      default: null
    },
    options: {
      type: Array as PropType<any[]>,
      required: false,
      default: []
    },
    valueName: {
      type: String as PropType<string>,
      required: false,
      default: 'value'
    },
    labelName: {
      type: String as PropType<string>,
      required: false,
      default: 'label'
    },
    placeholder: {
      type: String as PropType<string>,
      required: false,
      default: ''
    }
  },
  setup(props, { attrs }) {
    const isSelected = computed(() => props.value != null);

    return () => (
      <BaseSelect>
        {!isSelected.value ? <Placeholder>{props.placeholder}</Placeholder> : undefined}
        <Selector value={props.value} vOn:change={(e: any) => props.change?.(e.target.value)} {...{ ...attrs }}>
          <option value="" disabled />
          {props.options.map((option) => (
            <option value={option[props.valueName]}>{option[props.labelName]}</option>
          ))}
        </Selector>
      </BaseSelect>
    );
  }
});
