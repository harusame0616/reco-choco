import dayjs from 'dayjs';
import { v4 } from 'uuid';

export interface ModifyData {
  taskId?: string;
  beginTime?: Date;
  finishTime?: Date;
  comment?: string;
}

export class Chank {
  constructor(
    private _id: string,
    private _taskId: string,
    private _beginTime: Date,
    private _finishTime?: Date,
    private _comment: string = ''
  ) {
    this.beginTime = _beginTime;
    if (_finishTime) {
      this._finishTime = _finishTime;
    }
  }

  get comment() {
    return this._comment;
  }

  set comment(comment: string) {
    const maxLength = 120;
    if (comment.length >= maxLength) {
      throw new RangeError(`コメントは${maxLength}文字以内です`);
    }
    this._comment = comment;
  }

  get id() {
    return this._id;
  }

  get taskId() {
    return this._taskId;
  }

  set taskId(taskId: string) {
    this._taskId = taskId;
  }

  get beginTime() {
    return this._beginTime;
  }

  set beginTime(beginTime: Date) {
    const _beginTime = dayjs(beginTime).startOf('minute').toDate();
    if (this._finishTime && _beginTime > this._finishTime) {
      throw new RangeError('開始時間が終了時間より後です');
    }

    this._beginTime = _beginTime;
  }
  get finishTime() {
    return this._finishTime;
  }

  set finishTime(finishTime: Date | undefined) {
    const _finishTime = dayjs(finishTime).startOf('minute').toDate();
    if (finishTime && this._beginTime > _finishTime) {
      throw new RangeError('終了時間が開始時間より前です');
    }

    this._finishTime = _finishTime;
  }

  finish() {
    this.finishTime = new Date();
  }

  get isFinished() {
    return !!this._finishTime;
  }

  modify({ taskId, beginTime, finishTime, comment }: ModifyData) {
    if (taskId) {
      this._taskId = taskId;
    }

    if (beginTime) {
      this.beginTime = beginTime;
    }

    if (finishTime) {
      this.finishTime = finishTime;
    }

    if (comment) {
      this.comment = comment;
    }
  }

  static create(taskId: string, start?: Date) {
    return new Chank(v4(), taskId, start ?? new Date());
  }
}
