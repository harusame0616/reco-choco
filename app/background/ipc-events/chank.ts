import { ipcMain } from 'electron';
import { ChankApplication } from '../domains/applications/chank/chank-application';
import { getRepository } from '../domains/infrastractures/repository';
import { ModifyData } from '../domains/models/chank/chank';

export const chankApplication = new ChankApplication(
  getRepository('task'),
  getRepository('chank'),
  getRepository('chankQuery')
);
ipcMain.handle('api-chank-start', async (_, { id }) => {
  return await chankApplication.start(id);
});

ipcMain.handle('api-chank-finish', async (_, { id }) => {
  return await chankApplication.finish(id);
});

ipcMain.handle('api-chank-get-detail', async (_, { id }) => {
  return await chankApplication.getDetail(id);
});

ipcMain.handle('api-chank-get-active-chank', async (_, {}) => {
  return await chankApplication.getActiveChank();
});

ipcMain.handle('api-chank-modify', async (_, { id, data }: { id: string; data: ModifyData }) => {
  return await chankApplication.modify(id, data);
});

ipcMain.handle('api-chank-list-by-date', async (_, { date }) => {
  return await chankApplication.listByDate(date);
});

ipcMain.handle('api-chank-delete', async (_, { id }) => {
  return await chankApplication.delete(id);
});
