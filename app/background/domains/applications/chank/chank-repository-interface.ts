import { Chank } from '../../models/chank/chank';

export interface ChankRepositoryInterface {
  findOneById(id: string): Promise<Chank | undefined>;
  findByTaskId(id: string): Promise<Chank[]>;
  insert(chank: Chank): Promise<void>;
  list(): Promise<Chank[]>;
  update(chank: Chank): Promise<void>;
  deleteById(id: string): Promise<void>;
}
