import { ChankRepositoryInterface } from '../../applications/chank/chank-repository-interface';
import { Chank } from '../../models/chank/chank';

export class OMChankRepository implements ChankRepositoryInterface {
  async findByDate(date: Date): Promise<Chank[]> {
    return this._chanks.filter(
      (chank) => chank.beginTime <= date && (chank.finishTime == null || chank.finishTime >= date)
    );
  }
  public _chanks: Chank[] = [];
  async findOneById(id: string): Promise<Chank | undefined> {
    return this._chanks.find((chank) => chank.id === id);
  }
  async findByTaskId(id: string): Promise<Chank[]> {
    return this._chanks.filter((chank) => chank.taskId === id);
  }
  async insert(chank: Chank): Promise<void> {
    this._chanks.push(chank);
  }
  async list(): Promise<Chank[]> {
    return this._chanks;
  }
  async update(chank: Chank): Promise<void> {
    const i = this._chanks.findIndex((_chank) => _chank.id === chank.id);
    if (i < 0) {
      throw new Error('チャンクが見つかりません');
    }

    this._chanks[i] = chank;
  }
  async deleteById(id: string): Promise<void> {
    const i = this._chanks.findIndex((chank) => chank.id === id);
    if (i < 0) {
      return;
    }
    this._chanks.splice(i, 1);
  }
}
