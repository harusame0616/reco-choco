import { TaskDTO, taskToDto } from '../../applications/task/task-application';
import { TaskRepositoryInterface } from '../../applications/task/task-repository-interface';
import { Task } from '../../models/task/task';

export const taskFromDto = (taskDto: TaskDTO | undefined) => {
  return taskDto && taskDto.id ? new Task(taskDto.id, taskDto.name, taskDto.order) : undefined;
};

export class OMTaskRepository implements TaskRepositoryInterface {
  public _tasks: TaskDTO[];
  constructor(tasks?: Task[]) {
    this._tasks = tasks?.map((task) => taskToDto(task)) ?? [];
  }

  async findOneByLastOrder(): Promise<Task | undefined> {
    return taskFromDto(
      this._tasks.sort((a, b) => {
        if (a.order === b.order) {
          return 0;
        }

        return a.order > b.order ? -1 : 1;
      })[0]
    );
  }

  async findOneById(id: string) {
    return taskFromDto(this._tasks.find((task) => task.id === id));
  }

  async findOneByName(name: string) {
    return taskFromDto(this._tasks.find((task) => task.name === name));
  }

  async list(): Promise<Task[]> {
    return this._tasks.map((taskDto) => taskFromDto(taskDto) as Task);
  }

  async insert(task: Task) {
    this._tasks.push(taskToDto(task));
  }

  async update(task: Task) {
    const taskById = this._tasks.find(({ id }) => task.id === id);
    if (!taskById) {
      throw new Error('タスクが見つかりません');
    }

    taskById.name = task.name;
    taskById.order = task.order;
  }
  async deleteById(id: string) {
    const index = this._tasks.findIndex((task) => task.id === id);
    this._tasks.splice(index, 1);
  }

  async find() {
    return this._tasks.map((taskDto) => taskFromDto(taskDto));
  }
}
