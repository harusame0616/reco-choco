import { defineComponent } from '@nuxtjs/composition-api';

export default defineComponent({
  setup() {
    return () => (
      <div>
        <nuxt-link to="/aggregation">集計</nuxt-link>
        <nuxt-link to="/task">task管理</nuxt-link>
      </div>
    );
  }
});
