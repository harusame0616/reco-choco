import { Chank } from '../../models/chank/chank';
import { TaskRepositoryInterface } from '../task/task-repository-interface';
import { ChankRepositoryInterface } from './chank-repository-interface';

export class ChankDomainService {
  constructor(private _taskRepository: TaskRepositoryInterface, private _chankRepository: ChankRepositoryInterface) {}
  async canStart(chank: Chank) {
    const [taskById, chankById] = await Promise.all([
      this._taskRepository.findOneById(chank.taskId),
      this._chankRepository.findOneById(chank.id)
    ]);

    if (!taskById) {
      throw new Error('タスクが見つかりません');
    }
    if (chankById) {
      throw new Error('開始済みのチャンクです');
    }
  }

  async canModify(chank: Chank) {
    const taskById = await this._taskRepository.findOneById(chank.taskId);

    if (!taskById) {
      throw new Error('タスクが見つかりません');
    }
  }
}
