export interface ChankQueryInterface {
  findByDate(date: Date): Promise<any>;
  findById(id: string): Promise<any>;
  findLeatest(): Promise<any>;
}
