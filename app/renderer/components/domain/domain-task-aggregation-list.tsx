import { defineComponent, inject, PropType } from '@nuxtjs/composition-api';
import styled from 'vue-styled-components';
import { useChankAggregation } from '~/composables/use-chank-aggregation';

const CardWrap = styled.div`
  margin: 5px;
`;

export default defineComponent({
  props: {
    injectionKey: {
      type: Symbol as PropType<Symbol>,
      required: true
    }
  },
  setup(props) {
    const { tasks, deleteChank, editChank } = inject(props.injectionKey) as ReturnType<typeof useChankAggregation>;

    return () => (
      <div>
        {tasks.value
          .filter(({ chanks }) => chanks.length)
          .map((task) => (
            <CardWrap>
              <domain-task-aggregation-card
                task-aggregation={task}
                delete-chank={(id: string) => deleteChank(id)}
                edit-chank={(id: string) => editChank(id)}
                key={task.taskId}
              />
            </CardWrap>
          ))}
      </div>
    );
  }
});
